---
title: Material Math
description: Material Math brings unlimited mental math practice in a fun, beautiful interface to the web!
category: App Collection
position: 3001
---

### The spiritual successor to Boltz

Material Math is the spiritual successor to Boltz, a mental math app for Android. Grey software is building off of
Boltz's great work, and bringing Boltz's principles of unlimited fun math practice, interleaved concepts, and spaced
repition to the web!

<cta-button link="https://material-math.grey.software/" text="Demo"></cta-button>
<cta-button link="https://gitlab.com/grey-software/nest/Material-Math" text="Repo"></cta-button>

<br></br>

<youtube-video id="ceACiAdXSDc"></youtube-video>
