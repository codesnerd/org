---
title: Education Programs
description: An overview of Grey Software's revenue channels.
category: Revenue Channels
position: 2004
---

## Overview

Sign contracts with universities and coding bootcamps for our software education programs.

### Value proposition

> **For** (target customers)  
> **Who are dissatisfied with** (the current alternative)  
> **Our product is a** (new product)  
> **That provides** (essential problem-solving capability)  
> **Unlike** (the product alternative)

**For** university students

**Who are dissatisfied** with how their outdated curriculums are not effectively preparing them to create or scale
software products

**Grey Software is** an organization of passionate software creators and educators

Who build open products and publish educational content to **help students worldwide learn by example**

Unlike universities that **can't teach students** how to build up-to-date software products **because they aren't in the
business of creating products**

### Strategy

- Build open products and publish educational content with small groups of students
- Refine educational content through testing at university workshops
- Run small-scale collaboration programs with universities that will allow us to research and develop our programs
- Select for lifelong learners likely to come back as software creators and educators
- Achieve exponential scale organically through a passionate and skilled alumni network

## Customers and Partners

### Who are your personas?

#### Arsala - 2nd Year CS at UofT Canada

Arsala is a computer science student who is passionate about building apps that help people solve problems.

He has done well on school assignments but he wants to learn how to build useful software for people, and he's
frustrated with the pace of his university curriculum.

Arsala has software ideas that he wants to pursue in his free time, but he doesn't get far because of the long learning
paths, scattered information, and lack of guidance and structure.

Arsala wishes there was more structured guidance and mentorship available so he can bring his software ideas to life and
pursue his potential career paths as a software professional or entrepreneur.

#### Laiba -- 4th Year CS at UETM Pakistan

Laiba is about to graduate with a CS degree and pursue further education, but she wishes she had more practical project
experience than just her school-mandated final year project.

Laiba interned at a software company before returning for her final year at school, and she wishes that there were more
programs and opportunities for her to grow as a capable software professional during the initial years.

### What highlights from customer feedback validate the effectiveness of your go-to-market approach and business model?

I have been programming on the side for the past year now, mainly using online resources to pair-program... yet one
session with my mentor was more enriching than hours spent on these side tutorials. - **Osama, Grey Software Explorer**

I got to experience what it was like being onboarded into a codebase and collaborating with a skilled developer and
designer. I got the software development education I was looking for." - **Milind, Grey Software Apprentice**

This course simulated what it will be like to work in the industry by encouraging the students to work asynchronously
and proactively rather than following the instructions of an assignment. - **Shawn, Student at UTM**

I had to get used to a different mindset; working asynchronously and making progress daily instead of cramming before a
deadline. - **Lee, Student at UTM**

The most useful was the overall workflow because it can be applied to any future project. All projects need proper
documentation and remote repo, and group members need to work asynchronously by managing branches, pull requests, issues
and milestones. **- Baichen, Student at UTM**
