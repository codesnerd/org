---
title: Ecosystem Partners
description: An overview of Grey Software's revenue channels.
category: Revenue Channels
position: 2002
---

### Overview

We aim to generate revenue by offering 'Ecosystem Partner' sponsorship packages to organizations that want to feature
their brand on our websites and project repositories.

### Motivation

We have numerous websites throughout our ecosystem upon which we can place company branding. This advertising space is
the perfect offering for corporate sponsors who back us on Opencollective and Github.

### Is this urgent?

Yes, because we need to generate far greater revenue for the organization soon to meet our objectives. Corporate
sponsorship plans generate more revenue than individual sponsorships.

### Value proposition

For brands looking to advertise on platforms that reach software learners around the world, Grey Software builds open
products and publishes educational content for university students and young software entrepreneurs worldwide across an
ecosystem of public websites, repos, and media content.

### Strategy

- Create open education websites to share original and curated software knowledge
- Crowdsource the latest and greatest information by making it simple for professionals and academics to contribute
- Organize and publish up-to-date educational content about software technology on our websites
- Earn brand loyalty as a thought leader in software creation and education from software learners worldwide
- Generate revenue by offering sponsorship packages to organizations that want to feature their brand on our websites
  and project repositories.

### Decision Makers

#### Jane - Marketing Pro at a Software Company

Jane is a marketing professional at a software company that wants to raise awareness about its brand amongst the
open-source software community. Her company has already sponsored projects like Webpack, Vue, and Nuxt, and they would
likely partner with an open-source organization that demonstrates impressive analytics throughout its websites and
repositories.
